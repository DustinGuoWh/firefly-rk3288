# hardware/alsa_sound/Android.mk
#
# Copyright 2008 Wind River Systems
#
ifndef TARGET_BOARD_HARDWARE
TARGET_BOARD_HARDWARE := rk29sdk
endif
ifeq ($(strip $(BOARD_USES_ALSA_AUDIO)),true)
LIBAUDIO_HAREWARE_PATH := ../libhardware_legacy/audio/
#########################################################################
#old is libaudio.so, now add copy from libaudiohw_legacy
#########################################################################
  LOCAL_PATH := $(call my-dir)

  include $(CLEAR_VARS)

  LOCAL_ARM_MODE := arm
  LOCAL_CFLAGS := -D_POSIX_SOURCE
  LOCAL_CFLAGS+=-DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)
#  LOCAL_CFLAGS += -DALSA_LRSWITCH  
#  LOCAL_CFLAGS += -DALSA_VOLGAIN 
#  LOCAL_CFLAGS += -DIN_NO_OPS 
  LOCAL_C_INCLUDES += external/alsa-lib/include \
    $(call include-path-for, speex)

  LOCAL_SRC_FILES := \
    AudioHardwareALSA.cpp \
	AudioStreamOutALSA.cpp \
	AudioStreamInALSA.cpp \
	ALSAStreamOps.cpp \
	ALSAMixer.cpp \
	ALSAControl.cpp \
    $(LIBAUDIO_HAREWARE_PATH)/AudioHardwareInterface.cpp \
    audio_hw_hal.cpp

  LOCAL_MODULE := audio.primary.$(TARGET_BOARD_HARDWARE)
  LOCAL_MODULE_TAGS := optional
  LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

  LOCAL_STATIC_LIBRARIES += \
    libmedia_helper \
    libspeex

  LOCAL_SHARED_LIBRARIES := \
    libasound \
    libcutils \
    libutils \
    libmedia \
    libhardware \
    libhardware_legacy \
    libc

ifeq ($(BOARD_HAVE_BLUETOOTH),true)
#  LOCAL_SHARED_LIBRARIES += liba2dp
endif

  include $(BUILD_SHARED_LIBRARY)
  
#########################################################################
# copy from libaudiopolicy_legacy
#########################################################################
  include $(CLEAR_VARS)

  LOCAL_SRC_FILES := \
    AudioPolicyManagerBase.cpp \
    $(LIBAUDIO_HAREWARE_PATH)/AudioPolicyCompatClient.cpp \
    $(LIBAUDIO_HAREWARE_PATH)/audio_policy_hal.cpp

  ifeq ($(AUDIO_POLICY_TEST),true)
    LOCAL_CFLAGS += -DAUDIO_POLICY_TEST
  endif

  ifeq ($(BOARD_HAVE_BLUETOOTH),true)
    LOCAL_CFLAGS += -DWITH_A2DP
  endif

  LOCAL_STATIC_LIBRARIES := libmedia_helper
  LOCAL_MODULE := libaudiopolicy_$(TARGET_BOARD_HARDWARE)
  LOCAL_MODULE_TAGS := optional
  LOCAL_SHARED_LIBRARIES := \
    audio.primary.$(TARGET_BOARD_HARDWARE)

  include $(BUILD_STATIC_LIBRARY)
#########################################################################
# The default audio policy, for now still implemented on top of legacy
# policy code
#########################################################################
  include $(CLEAR_VARS)

  LOCAL_SRC_FILES := \
    $(LIBAUDIO_HAREWARE_PATH)/AudioPolicyManagerDefault.cpp

  LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils \
    libmedia

  LOCAL_STATIC_LIBRARIES := \
    libmedia_helper

  LOCAL_WHOLE_STATIC_LIBRARIES := \
    libaudiopolicy_$(TARGET_BOARD_HARDWARE)

  LOCAL_MODULE := audio_policy.$(TARGET_BOARD_HARDWARE)
  LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
  LOCAL_MODULE_TAGS := optional

  ifeq ($(BOARD_HAVE_BLUETOOTH),true)
    LOCAL_CFLAGS += -DWITH_A2DP
  endif

  include $(BUILD_SHARED_LIBRARY)
#########################################################################
# This is the default ALSA module which behaves closely like the original
#########################################################################
  include $(CLEAR_VARS)

  LOCAL_PRELINK_MODULE := false

  LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

  LOCAL_CFLAGS := -D_POSIX_SOURCE -Wno-multichar

ifneq ($(ALSA_DEFAULT_SAMPLE_RATE),)
    LOCAL_CFLAGS += -DALSA_DEFAULT_SAMPLE_RATE=$(ALSA_DEFAULT_SAMPLE_RATE)
endif

  LOCAL_CFLAGS+=-DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)
  
  LOCAL_C_INCLUDES += external/alsa-lib/include \
    $(call include-path-for, speex)

  LOCAL_SRC_FILES:= alsa_default.cpp

  LOCAL_SHARED_LIBRARIES := \
  	libasound \
  	liblog \
	libutils \
	libcutils

  LOCAL_MODULE_TAGS := optional
  LOCAL_MODULE:= alsa.default

  include $(BUILD_SHARED_LIBRARY)
#########################################################################
# This is the default Acoustics module which is essentially a stub
#########################################################################
  include $(CLEAR_VARS)

  LOCAL_PRELINK_MODULE := false

  LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

  LOCAL_CFLAGS := -D_POSIX_SOURCE -Wno-multichar
  LOCAL_CFLAGS+=-DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)
  
  LOCAL_C_INCLUDES += external/alsa-lib/include \
    $(call include-path-for, speex)

  LOCAL_SRC_FILES:= acoustics_default.cpp

  LOCAL_SHARED_LIBRARIES := liblog

  LOCAL_MODULE_TAGS := optional
  LOCAL_MODULE:= acoustics.default

  include $(BUILD_SHARED_LIBRARY)

endif
